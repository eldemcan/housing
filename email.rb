require 'google/apis/gmail_v1'
require 'googleauth'
require 'googleauth/stores/file_token_store'
require 'fileutils'
require 'rmail'
require 'tempfile'

class EmailService
   OOB_URI = "urn:ietf:wg:oauth:2.0:oob".freeze
   APPLICATION_NAME = "Gmail API Ruby Quickstart".freeze
   CREDENTIALS_PATH = "credentials.json".freeze
   TOKEN_PATH = "token.yaml".freeze
   SCOPE = Google::Apis::GmailV1::AUTH_SCOPE
   GMAIL = Google::Apis::GmailV1

  attr_accessor :service
  def initialize
    @service = GMAIL::GmailService.new
    @service.client_options.application_name = APPLICATION_NAME
    @service.authorization = authorize
  end


  def send_mail(body)
    message = RMail::Message.new
    message.header['To'] = ''
    message.header['From'] = 'eldemcan@gmail.com'
    message.header['Subject'] = "Daft update #{Date.today.to_s}"
    message.header.add('Content-Type', 'text/html')
    message.body = body
    service.send_user_message('me',
                            upload_source: StringIO.new(message.to_s),
                            content_type: 'message/rfc822')
  end

  private
  ##
  # Ensure valid credentials, either by restoring from the saved credentials
  # files or intitiating an OAuth2 authorization. If authorization is required,
  # the user's default browser will be launched to approve the request.
  #
  # @return [Google::Auth::UserRefreshCredentials] OAuth2 credentials
  def authorize
  # with environment variables
    client_id = nil
    if ENV['GOOGLE_CLIENT_ID'] && ENV['GOOGLE_CLIENT_SECRET']
      client_id = Google::Auth::ClientId.new(ENV['GOOGLE_CLIENT_ID'], ENV['GOOGLE_CLIENT_SECRET'])
    else
      client_id = Google::Auth::ClientId.from_file CREDENTIALS_PATH
    end

    token_store = Google::Auth::Stores::FileTokenStore.new file: token_file
    authorizer = Google::Auth::UserAuthorizer.new client_id, SCOPE, token_store
    user_id = "default"
    credentials = authorizer.get_credentials user_id
    if credentials.nil?
      url = authorizer.get_authorization_url base_url: OOB_URI
      puts "Open the following URL in the browser and enter the " \
           "resulting code after authorization:\n" + url
      code = gets
      credentials = authorizer.get_and_store_credentials_from_code(
        user_id: user_id, code: code, base_url: OOB_URI
      )
    end
    credentials
  end

  def token_file
    if ENV['token']
      file = Tempfile.new(TOKEN_PATH)
      content = <<-YAML
      ---
      #{ENV['token']}
      YAML

      file.write(content.strip)
      file.rewind

      file.path
    else
      TOKEN_PATH
    end
  end
end
