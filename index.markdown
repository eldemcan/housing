---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
---

<link rel="stylesheet" href="{{site.baseurl}}/assets/css/index.css">

<p> Total House: {{site.data.index.size}} </p>

{% leaflet_map { "zoom" : 9,
                 "divId" : "mydiv123" } %}

{% for item in site.data.index %}
{% capture content %}
"<div> <p> Price: {{item.price}} Beds: {{item.beds}} Baths: {{item.baths}} </p> <a href=https://www.google.com/maps/search/?api=1&query={{item.coordinates[1]}},{{item.coordinates[0]}} > maps </a> <a href={{item.link}}> daft </a> <img src={{item.img_url}}> </div>"
{% endcapture %}

{% leaflet_marker { "latitude" : {{ item.coordinates[1] }},
                       "longitude" : {{ item.coordinates[0] }},
                       "leafletMarkerIcon": 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-green.png',
                       "popupContent" : {{content}}  } %}
{% endfor %}
{% endleaflet_map %}

